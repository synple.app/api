module Modusynth
  module Services
    module Concerns
      autoload :Creator, './services/concerns/creator'
      autoload :Deleter, './services/concerns/deleter'
      autoload :Finder, './services/concerns/finder'
      autoload :Updater, './services/concerns/updater'
    end
  end
end
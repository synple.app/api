json.id synthesizer.id.to_s
json.(synthesizer, :name, :slots, :racks, :x, :y, :scale)
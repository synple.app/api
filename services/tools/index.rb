module Modusynth
  module Services
    module Tools
      autoload :Categories, './services/tools/categories'
      autoload :Controls, './services/tools/controls'
      autoload :Create, './services/tools/create'
      autoload :Delete, './services/tools/delete'
      autoload :Descriptors, './services/tools/descriptors'
      autoload :Find, './services/tools/find'
      autoload :InnerNodes, './services/tools/inner_nodes'
      autoload :LinkEnds, './services/tools/link_ends'
      autoload :Links, './services/tools/links'
      autoload :Parameters, './services/tools/parameters'
      autoload :Ports, './services/tools/ports'
      autoload :Update, './services/tools/update'
    end
  end
end